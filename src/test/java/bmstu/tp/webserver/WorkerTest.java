package bmstu.tp.webserver;

import junit.framework.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 */
@Test
public class WorkerTest {
    private Worker worker;

    @BeforeMethod
    public void setUp() throws Exception {
        worker = new Worker();
    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testNormal() throws Exception {
        String query = "GET / HTTP/1.0\r\n";
        InputStream inputStream = new ByteArrayInputStream(query.getBytes("UTF-8"));
        OutputStream outputStream = new ByteArrayOutputStream();
        worker.proceedRequest(inputStream, outputStream);
        Assert.assertTrue(outputStream.toString().contains("Hello, World"));
    }

    @Test
    public void test404() throws Exception {
        String query = "GET /some_non-existing_path HTTP/1.0\r\n";
        InputStream inputStream = new ByteArrayInputStream(query.getBytes("UTF-8"));
        OutputStream outputStream = new ByteArrayOutputStream();
        worker.proceedRequest(inputStream, outputStream);
        Assert.assertTrue(outputStream.toString().contains("404 Not Found"));
    }

    @Test
    public void test400() throws Exception {
        String query = "GET /../../secret_file HTTP/1.0\r\n";
        InputStream inputStream = new ByteArrayInputStream(query.getBytes("UTF-8"));
        OutputStream outputStream = new ByteArrayOutputStream();
        worker.proceedRequest(inputStream, outputStream);
        Assert.assertTrue(outputStream.toString().contains("400 Bad Request"));
    }

    @Test
    public void testPost() throws Exception {
        String query = "POST / HTTP/1.0\r\nContent-Length: 9\r\n\r\nsome data";
        InputStream inputStream = new ByteArrayInputStream(query.getBytes("UTF-8"));
        OutputStream outputStream = new ByteArrayOutputStream();
        worker.proceedRequest(inputStream, outputStream);
        Assert.assertTrue(outputStream.toString().contains("POST method is not supported"));
    }
}
