package bmstu.tp.webserver;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 */
@Test
public class FileTest {

    private File file;
    private byte[] data = new byte[] {1, 2, 3, 4, 5};
    private String contentType = "application/octet-stream";

    @BeforeMethod
    public void setUp() {
        file = new File(data, data.length, contentType);
    }

    @Test
    public void testContents() {
        Assert.assertEquals(file.getContents(), data);
    }

    @Test
    public void testLength() {
        Assert.assertEquals(file.getLength(), data.length);
    }

    @Test
    public void testContentType() {
        Assert.assertEquals(file.getContentType(), contentType);
    }
}
