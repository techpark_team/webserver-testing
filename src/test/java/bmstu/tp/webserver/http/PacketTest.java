package bmstu.tp.webserver.http;

import static org.testng.Assert.*;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created with IntelliJ IDEA.
 */
@Test
public class PacketTest {

    private Packet packet;

    @BeforeMethod
    public void setUp() throws Exception {
        packet = new Packet();
    }


    @Test
    public void testDefaultVersion() throws Exception {
        assertEquals(packet.getVersion(), Version.HTTP10);
    }

    @Test
    public void testCustomVersion() throws Exception {
        packet.setVersion(Version.HTTP11);
        assertEquals(packet.getVersion(), Version.HTTP11);
    }

    @Test
    public void testNotExistingHeader() throws Exception {
        assertNull(packet.getHeader("someFakeHeader"));
    }

    @Test
    public void testExistingHeader() throws Exception {
        packet.setHeader("header1", "value1");
        packet.setHeader("header2", "value2");
        assertEquals(packet.getHeader("header1"), "value1");
        assertEquals(packet.getHeader("header2"), "value2");
    }

    @Test
    public void testBody() throws Exception {
        byte[] body = new byte[] {1, 2, 3, 4, 5};
        packet.setBody(body);
        assertEquals(packet.getBody(), body);
    }
}
