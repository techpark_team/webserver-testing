package bmstu.tp.webserver.http;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 */
@Test
public class RequestTest {

    private Request request;

    @BeforeMethod
    public void setUp() throws Exception {
        request = new Request();
    }

    @Test(expectedExceptions = RequestFormatException.class)
    public void testWrongHttpVersion() throws Exception {
        String line = "GET / HTTP/4.0";
        request.parseInitialLine(line);
    }

    @Test
    public void testHttp10() throws Exception {
        String line = "GET / HTTP/1.0";
        request.parseInitialLine(line);
        assertEquals(request.getVersion(), Version.HTTP10);
    }

    @Test
    public void testHttp11() throws Exception {
        String line = "GET / HTTP/1.1";
        request.parseInitialLine(line);
        assertEquals(request.getVersion(), Version.HTTP11);
    }

    @Test
    public void testUri() throws Exception {
        request.parseInitialLine("GET /foo/bar HTTP/1.0");
        assertEquals(request.getUri(), "/foo/bar");
    }

    @Test
    public void testUriWithParameters() throws Exception {
        request.parseInitialLine("GET /foo/bar?foo=123&bar=456 HTTP/1.0");
        assertEquals(request.getUri(), "/foo/bar");
    }

    @Test
    public void testMethodGET() throws Exception {
        request.parseInitialLine("GET / HTTP/1.1");
        assertEquals(request.getMethod(), Method.GET);
    }

    @Test
    public void testMethodPOST() throws Exception {
        request.parseInitialLine("POST / HTTP/1.1");
        assertEquals(request.getMethod(), Method.POST);
    }

    @Test
    public void testHeadersParser() throws Exception {
        request.parseHeaderLine("Content-type: text/html");
        assertEquals(request.getHeader("Content-type"), "text/html");
    }
}
