package bmstu.tp.webserver.http;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 */
public class ResponseTest {

    private Response response;

    @BeforeMethod
    public void setUp() throws Exception {
        response = new Response();
    }

    @Test
    public void testHeaders() throws Exception {
        response.setHeader("Content-type", "text/html");
        response.setHeader("Content-Length", "1024");
        assertTrue(response.getHeaders()
                .contains("Content-type: text/html\r\nContent-Length: 1024"));
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testWrongStatusCode() throws Exception {
        response.setStatusCode(666);
    }

    @Test
    public void testStatusCode() throws Exception {
        response.setStatusCode(400);
        assertTrue(response.getHeaders().contains("400"));
        assertTrue(response.getHeaders().contains("Bad Request"));
    }
}
